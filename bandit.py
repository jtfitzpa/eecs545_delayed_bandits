#!/usr/bin/env python3

import numpy as np
import scipy as sio
import matplotlib as mpl
import matplotlib.pyplot as plt
import pylab as pl


np.random.seed(0)


class bandits():
	"""
	Generate bandit data.

	Defaults:
    K=4 arms
	D=4 contexts
	"""
	def __init__(self, K=4, D=4, m=250, eta=0, rounds=2000, c=0.002, mode="syn"):

		self.D = D # dimension of the feature vector
		self.K = K # number of bandits
		self.arm_means = np.repeat(np.random.normal(0.5,0.1),self.K) #shows which arm would have the best rewards, i.e. the highest expected reward
		self.arm_stds = 0.1*np.random.rand(self.K)#np.absolute(np.random.normal(0, 0.1, size=self.K))
		self.rounds = rounds
		self.m = m
		#this is used later to get a random noise factor eta
		self.eta_means = eta
		#some constants used for f(t) later, just setting it to 1 to make everything easy I guess?
		self.c = c
		#generate the weight vectors.  initialize estimate of feature
		self.get_weights()
		#get the condition variable Ct, can be generated ahead of time
		self.Ct = self.get_conditions()
		#generate a model for the delay
		self.delay = self.get_delay()
		self.all_contexts = np.zeros((self.rounds, self.D, self.K))
		self.all_actions = np.zeros(self.rounds, int)
		self.all_rewards = np.zeros(self.rounds)

		self.regret = np.zeros(self.rounds)

		if(mode == "syn"):
			self.theta_opt = np.random.normal(0, 0.5, (self.D, 1))

	def get_weights(self, mu=0.5, sig=0.3):
		self.theta = np.random.normal(mu,sig,size=(self.D, 1))

	def f_t_delta(self, t):
		delta = (1/self.rounds + 1)/2
		ft = 2 * (1 + 1/np.log(t)) * np.log(1/delta) + self.c*self.D*np.log(self.D*np.log(t))
		return ft

	def get_conditions(self, pc=0.75):
		return np.random.binomial(1, pc, size=self.rounds)

	def get_delay(self, mu=10, sig=2):
		#return np.zeros(self.rounds)
		return np.random.geometric(0.01,size=self.rounds)
		#return np.random.normal(mu, sig, size=self.rounds)

	def get_context(self):
		#return sim_data(self.K,self.D)
		return np.random.normal(self.arm_means, self.arm_stds, (self.D, self.K))

	def get_reward(self, action_idx, t):
		#At = get_context)
		At = self.all_contexts[t, :, action_idx]
		Xt = self.theta_opt.T @ At + np.random.normal(self.eta_means, 0.1)
		#self.all_contexts[t] = At
		return Xt

	def get_true_reward(self, t, s):
		Yst = self.Ct[s] * (self.delay[s] <= np.minimum(self.m, t-s)) * self.all_rewards[s]
		return Yst

	def get_regret(self):
		regret = np.zeros(self.rounds)
		#print(self.all_actions)
		for i in range(0, self.rounds):
			A_opt = np.amax(self.theta_opt.T @ self.all_contexts[i])
			#print("Aopt is ", A_opt)
			regret[i] = A_opt - self.theta_opt.T @ self.all_contexts[i, :, np.int(self.all_actions[i])]
		return np.cumsum(regret)

	def theta_disc(self, t):
		V = np.zeros([self.D, self.D])
		b = np.zeros((self.D, 1))
		assert(t - self.m - 1 >= 0)
		for i in range(0, t-self.m-1):
			#print(self.all_actions[i])
			V += np.outer(self.all_contexts[i, :, np.int(self.all_actions[i])], self.all_contexts[i, :, np.int(self.all_actions[i])])
			b +=  self.get_true_reward(t, i)*self.all_contexts[i, :, np.int(self.all_actions[i])].reshape(20,1)
		t_disc = np.linalg.pinv(V) @ b
		return t_disc, V

	def theta_b(self, t):
		V = np.zeros([self.D, self.D])
		b = np.zeros((self.D,1))
		assert(t - 1 > 0)
		for i in range(0, t - 1):
			V += np.outer(self.all_contexts[i, :, np.int(self.all_actions[i])], self.all_contexts[i, :, np.int(self.all_actions[i])])
			b += self.get_true_reward(t, i) * self.all_contexts[i, :, np.int(self.all_actions[i])].reshape(20,1)
		t_b = np.linalg.pinv(V) @ b
		return t_b, V

	#update At for WaiLinUCB
	def wai_At(self, t_disc, V, t):
		return np.argmax(self.all_contexts[t].T @ t_disc + 3* np.sqrt(np.diag(self.all_contexts[t].T @ np.linalg.pinv(V) @
			self.all_contexts[t]).reshape((self.K, 1)) * self.f_t_delta(t=t)))

	#update At for DeLinUCB
	def de_At(self, t_b, V, t):
		#print((self.all_contexts[t].T @ t_b).shape)
		V_pseudo = np.linalg.pinv(V)
		return np.argmax(self.all_contexts[t].T @ t_b +  np.sqrt(np.diag(self.all_contexts[t].T @ V_pseudo @
			self.all_contexts[t]).reshape((self.K, 1)) * self.f_t_delta(t=t)) +  self.m/100 * np.sqrt(np.diag(self.all_contexts[t].T @ V_pseudo @
			V_pseudo @ self.all_contexts[t]).reshape(self.K, 1)))


    # WaiLinUCB
	def run_wailin(self):
		for i in range(0, self.rounds):
			if (i <= self.m + 1):
				A = self.get_context()
				self.all_contexts[i] = A
				action_idx = np.random.randint(0, self.K)
				#print("action is ", action_idx)
				self.all_rewards[i] = self.get_reward(action_idx, i)
				self.all_actions[i] = action_idx
			else:
				A = self.get_context()
				self.all_contexts[i] = A
				self.theta, V = self.theta_disc(i)
				action_idx = self.wai_At(self.theta, V, i)
				self.all_actions[i] = action_idx
				self.all_rewards[i] = self.get_reward(action_idx, i)
		print(np.linalg.norm(self.theta_opt - self.theta))
		return self.get_regret()

    # DeLinUCB
	def run_delin(self):
		for i in range(0, self.rounds):
			if (i <= 1):
				A = self.get_context()
				self.all_contexts[i] = A
				action_idx = np.random.randint(0, self.K)
				reward = self.get_reward(action_idx, i)
				self.all_rewards[i] = reward
				self.all_actions[i] = action_idx
			else:
				A = self.get_context()
				self.all_contexts[i] = A
				self.theta, V = self.theta_b(i)
				action_idx = self.de_At(self.theta, V, i)
				#print(action_idx.shape)
				self.all_actions[i] = action_idx
				self.all_rewards[i] = self.get_reward(action_idx, i)
		print(np.linalg.norm(self.theta_opt - self.theta))
		return self.get_regret()

def main():

	wai = bandits(K=10,D=20,rounds=2000)
	wai_regret = wai.run_wailin()
	de = bandits(K=10,D=20,rounds=2000)
	de_regret = de.run_delin()
	plt.plot(wai_regret)
	plt.plot(de_regret)
	plt.legend(['WaiLinUCB','DeLinUCB'])
	plt.xlabel('Time')
	plt.ylabel('Accumulated Regret')
	plt.show()


if __name__ == '__main__':
	main()
